package org.ibase4j.core.base;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class BaseModel implements Serializable {

	public Integer getId() {
		return null;
	}

	public void setId(Integer id) {
	}

	public Integer getEnable() {
		return null;
	}

	public void setEnable(Integer enable) {
	}

	public String getRemark() {
		return null;
	}

	public void setRemark(String remark) {
	}

	public Date getCreateTime() {
		return null;
	}

	public void setCreateTime(Date createTime) {
	}

	public Integer getCreateBy() {
		return null;
	}

	public void setCreateBy(Integer createBy) {
	}

	public Date getUpdateTime() {
		return null;
	}

	public void setUpdateTime(Date updateTime) {
	}

	public Integer getUpdateBy() {
		return null;
	}

	public void setUpdateBy(Integer updateBy) {
	}
}
